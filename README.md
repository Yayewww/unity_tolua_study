# Unity_tolua_study
## 腳本執行流程

## 1. 初始化 (LoadApp, FirebaseInit)
> App啟動 -> 進入Main Scene -> 檢測Firebase初始化
> + 初始化失敗
> > 進 __離線__ 流程
> + 初始化成功
> > 進 __連線__ 流程

## 2-1. 離線 (----)
> 進 __標題頁面__ 流程

## 2-2. 連線 (NetworkManager)
> __版本檢測__ 
> > 版本號檢測 -> 下載最新的關聯文件(manifest) -> 進 __AB檢測__ 流程 -> __標題頁面__ 流程

## 3. AB檢測 (ABLoader)
> 用最新的manifest提供的hash去調用此API，會比對AB的hash決定要從哪邊載入(快取/網路)

    UnityWebRequest.GetAssetBundle(url, hash, crc)

> 快取位置

    Application.persistentDataPath + "/UnityCache/Shared/"

> 檢查方式

    Caching.IsVersionCached(url, hash)

> 載入完畢後要將其從memory釋放

    request.Dispose()

## 4. 標題頁面
> 啟用Lua虛擬機，綁定好各遊戲控件、模塊，再點畫面進入遊戲主流程
> > 1. LuaManager.cs
> > + 設置LuaClient，啟動虛擬機運行遊戲物件載入。
> > 2. Main.lua 
> > + 宣告全局模塊變數(UnityEngine.XXX)，運行TitleView.lua
> > 3. TitleView.lua
> > + 從ABLoader載入對應UI.assetbundle，生成、設置參數，從memory釋放AB。
> > 4. 進入遊戲流程


## 其他
> 上傳的東西可以加入metadata, ex :
> 
        var metadata = MetaDataChange()
        {
            ContentEncoding = "image/png",
            CustomMetadata = new Dictionnary<string, string>
            {
                {"Position", Camera.main.transform.position.ToString()}
            }
        };

> 確認Google Play服務

        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
        var dependencyStatus = task.Result;
        if (dependencyStatus == Firebase.DependencyStatus.Available) {
            // Create and hold a reference to your FirebaseApp,
            // where app is a Firebase.FirebaseApp property of your application class.
            app = Firebase.FirebaseApp.DefaultInstance;

            // Set a flag here to indicate whether Firebase is ready to use by your app.
        } 
        else {
                UnityEngine.Debug.LogError(System.String.Format(
                "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.
            }
        });
> 讀取Lua實作，任何Lua內require到的對象都會跑一次下方的ReadFile

        public virtual byte[] ReadFile(string fileName)
        {
            if (!beZip)
            {
                string path = FindFile(fileName);
                byte[] str = null;

                if (!string.IsNullOrEmpty(path) && File.Exists(path))
                {
        #if !UNITY_WEBPLAYER
                    str = File.ReadAllBytes(path);
        #else
                    throw new LuaException("can't run in web platform, please switch to other platform");
        #endif
                }

                return str;
            }
            else
            {
                return ReadZipFile(fileName);
            }
        }
> LuaState啟動必Read的LuaFile

        tolua.lua
            //tolua require的對象如下
            functions.lua
            Mathf.lua
            Vector3.lua
            Quaternion.lua
            Vector2.lua
            Vector4.lua
            Color.lua
            Ray.lua
            Bounds.lua
            RaycastHit.lua
            Touch.lua
            LayerMask.lua
            Plane.lua
            Time.lua
            list.lua
            utf8.lua
            event.lua
            typeof.lua
            slot.lua
            Timer.lua
            coroutine.lua
            ValueType.lua
            BindingFlags.lua
            //Dofile
            Main.lua

> 引用插件時要用Resolver。

        Assets -> Android Resolver -> Force Resolve