using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using Firebase.Storage;

public class AssetbundleTool : MonoBehaviour
{
#if UNITY_STANDALONE
    public static string osDir = "Win";
#elif UNITY_ANDROID
    public static string osDir = "Android";            
#elif UNITY_IPHONE
    public static string osDir = "iOS";        
#else
    public static string osDir = "";        
#endif
    public static string version = "1.0.2";
    static string bundlePath = Application.streamingAssetsPath + "/" + osDir;
    static int totalFiles = 0;
    static int uploadedFiles = 0;
    static List<string> bundleNames;
    static List<string> bundleHashes;

    [MenuItem("TakaTools/Create BundleData in Local Path", false, 2)]
    static void CreateLocalBundleData()
    {
        bundleNames = new List<string>();
        bundleHashes = new List<string>();

        if (!Directory.Exists(bundlePath))
        {
            Debug.LogError("There's no path!!!!");
            return;
        }
        string[] filePaths = Directory.GetFiles(bundlePath);
        if (filePaths.Length < 0 || filePaths == null)
        {
            Debug.LogError("There's no bundle!!!!");
            return;
        }
        for (int i = 0; i < filePaths.Length; i++)
        {
            var path = filePaths[i];
            if (!IsMetaFile(path))
            {
                var fileName = path.Substring(path.LastIndexOf(@"\") + 1);//filePath out with "dir/dir\filename.subname", so i use @"\"
                if (!fileName.EndsWith(".manifest"))
                {
                    Hash128 hash;
                    BuildPipeline.GetHashForAssetBundle(path, out hash);
                    bundleNames.Add(fileName);
                    bundleHashes.Add(hash.ToString());
                }
            }
        }
        WriteBundleDataToJson(bundleNames, bundleHashes, false);
    }

    [MenuItem("TakaTools/Upload All Bundles And BundleData &u", false, 1)]
    static void UploadAllBundles()
    {
        //Init
        totalFiles = 0;
        uploadedFiles = 0;
        bundleNames = new List<string>();
        bundleHashes = new List<string>();

        if (!Directory.Exists(bundlePath))
        {
            Directory.CreateDirectory(bundlePath);
        }

        string[] filePaths = Directory.GetFiles(bundlePath);

        for (int i = 0; i < filePaths.Length; i++)
        {
            if (!IsMetaFile(filePaths[i]))
            {
                totalFiles += 1;
                UploadBundle(filePaths[i]);
            }
        }
        Debug.Log("Total Upload Files Number : " + totalFiles);
    }
    static async void UploadBundle(string path)
    {
        var storage = FirebaseStorage.DefaultInstance;
        var fileName = path.Substring(path.LastIndexOf(@"\") + 1);//filePath out with "dir/dir\filename.subname", so i use @"\"
        var bundleRef = storage.GetReference($"/patch/{osDir}/{fileName}");
        var new_metadata = new MetadataChange();//輸入metaData控制該文件在CDN上的生命週期
        new_metadata.CacheControl = "public,max-age=60";
        var uploadTask = bundleRef.PutFileAsync(path, new_metadata);

        Debug.Log($"Uploading bundle: {fileName}");
        await uploadTask;
        if (uploadTask.Exception != null)
        {
            Debug.LogError($"Failed to upload because {uploadTask.Exception}");
            return;
        }
        else
        {
            if (!fileName.EndsWith(".manifest"))
            {
                Hash128 hash;
                BuildPipeline.GetHashForAssetBundle(path, out hash);
                bundleNames.Add(fileName);
                bundleHashes.Add(hash.ToString());
            }

            uploadedFiles += 1;
            Debug.Log($"Upload Compeleted. {uploadedFiles} \n{fileName} ");
            //全部上傳完成後 
            if (uploadedFiles == totalFiles)
            {
                WriteBundleDataToJson(bundleNames, bundleHashes, true);
            }
        }
    }
    static bool IsMetaFile(string path)
    {
        return path.EndsWith(".meta");
    }
    static void WriteBundleDataToJson(List<string> names, List<string> hashes, bool upload)
    {
        BundleData data = new BundleData();
        data.nativeVersion = version;
        data.Names = names;
        data.Hashes = hashes;

        //Local Path
        string savePath = GlobalConfig.LOCAL_BUNDLEDATA_PATH;
        if (File.Exists(savePath))
        {
            File.Delete(savePath);
        }

        string json = JsonUtility.ToJson(data);
        Debug.Log("BundleData = \n"+json);
        File.WriteAllText(savePath, json);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        Debug.Log("Write BundleData Success");
        if (upload)
        {
            UploadBundleData(savePath);
        }
    }
    static async void UploadBundleData(string savePath)
    {
        //Upload File
        var storage = FirebaseStorage.DefaultInstance;
        var bundleRef = storage.GetReference($"/patch/BundleData/BundleData.json");
        var new_metadata = new MetadataChange();//輸入metaData控制該文件在CDN上的生命週期
        new_metadata.CacheControl = "public,max-age=60";
        var uploadTask = bundleRef.PutFileAsync(savePath, new_metadata);
        await uploadTask;
        if (uploadTask.Exception != null)
        {
            Debug.LogError($"Failed to upload because {uploadTask.Exception}");
        }
        else
        {
            Debug.LogError($"BundleData Uploaded");
            Debug.LogError("Total Bundles Uploaded!!!!!!");
        }
    }
}
