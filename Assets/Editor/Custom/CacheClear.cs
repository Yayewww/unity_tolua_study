﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace ETEditor
{
    public class CacheClear
    {
        [MenuItem("Tools/清空 Cache", false, 200)]
        static void CacheClearEditor()
        {
            if (EditorUtility.DisplayDialog("警告！", string.Format("確定清除本機Cache? 目前使用 {0} KB", Caching.currentCacheForWriting.spaceOccupied / 1024), "清除", "取消"))
            {
                Caching.ClearCache();
                Debug.Log($"清光光了 {Caching.currentCacheForWriting.spaceOccupied / 1024} KB");
            }
        }
        [MenuItem("Tools/刪除本地BundleData", false, 200)]
        static void ClearPersistentDataPath()
        {
            if (File.Exists(GlobalConfig.LOCAL_BUNDLEDATA_PATH))
            {
                File.Delete(GlobalConfig.LOCAL_BUNDLEDATA_PATH);
                Debug.Log("刪掉了本地BundleData");
            }
            else
            {
                Debug.Log("沒有本地BundleData");
            }
        }
        [MenuItem("Tools/Create Prefab")]
        static void CreatePrefab()
        {
            // Keep track of the currently selected GameObject(s)
            GameObject[] objectArray = Selection.gameObjects;

            // Loop through every GameObject in the array above
            foreach (GameObject gameObject in objectArray)
            {
                // Set the path as within the Assets folder,
                // and name it as the GameObject's name with the .Prefab format
                string localPath = "Assets/Res/Prefab/" + gameObject.name + ".prefab";

                // Make sure the file name is unique, in case an existing Prefab has the same name.
                localPath = AssetDatabase.GenerateUniqueAssetPath(localPath);

                // Create the new Prefab.
                PrefabUtility.SaveAsPrefabAssetAndConnect(gameObject, localPath, InteractionMode.UserAction);
            }
        }
    }
}