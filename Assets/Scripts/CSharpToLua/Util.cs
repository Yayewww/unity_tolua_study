using DG.Tweening;
using LuaInterface;
using System;
using System.IO;
using System.Threading.Tasks;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class Util : MonoBehaviour
{
    public static int Int(object o)
    {
        return Convert.ToInt32(o);
    }

    public static float Float(object o)
    {
        return (float)Math.Round(Convert.ToSingle(o), 2);
    }
    public static void DoMove(GameObject obj, Vector3 vec, float time)
    {
        obj.transform.DOMove(vec, time);
    }

    public static void DoMove(GameObject obj, Vector3 vec, float time, LuaFunction function)
    {
        obj.transform.DOMove(vec, time).OnComplete(() => 
        {
            function.Call(obj);
        });
    }
    public static void DoMoveAnchor(GameObject obj, Vector2 pos, float time, LuaFunction function, Ease type)
    {
        obj.GetComponent<RectTransform>().DOAnchorPos(pos, time).SetEase(type).OnComplete(() =>
        {
            function.Call(obj);
        });
    }
    public static void KillDoTween(GameObject obj)
    {
        obj.transform.DOKill();
    }
    public static void DoScale(GameObject obj, float f, float time, LuaFunction function)
    {
        obj.transform.DOScale(f, time);
    }

    public static void DoScale(GameObject obj, Vector3 vec, float time, LuaFunction function)
    {
        obj.transform.DOScale(vec, time);
    }
    public static async void DoFunctionDelay(GameObject obj, float seconds, LuaFunction function)
    {
        if (obj == null)
        {
            Debug.LogError("Lua DoFunctionDelay() obj is NULL");
            return;
        }
        int milliseconds = Mathf.RoundToInt((float)TimeSpan.FromSeconds(seconds).TotalMilliseconds);
        await Task.Delay(milliseconds);
        function.Call(obj);
    }
    public static void DestroyObject(GameObject obj)
    {
        Destroy(obj);
    }
    public static string ReadScore()
    {
        string score = "none";
        if (File.Exists(GlobalConfig.LOCAL_SAVE_DATA_PATH))
        {
            StreamReader sr = new StreamReader(GlobalConfig.LOCAL_SAVE_DATA_PATH);
            score = sr.ReadLine();
            sr.Close();
        }
        return score;
    }
    public static void SaveScore(string score)
    {
        FileStream fs = new FileStream(GlobalConfig.LOCAL_SAVE_DATA_PATH, FileMode.Create, FileAccess.ReadWrite);
        StreamWriter sw = new StreamWriter(fs);
        sw.Write(score);
        sw.Close();
    }
    public static void SignInGoogle()
    {
        FirebaseManager.Instance.SignInGoogle();
    }
    public static void SignOutGoogle()
    {
        FirebaseManager.Instance.SignOutGoogle();
    }
}
