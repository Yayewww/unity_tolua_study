using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LuaInterface;


public class ObjectToLua : MonoBehaviour
{
    [Header("Lua檔不加副檔名")]
    public string luaName = "xxx";
    void Start()
    {
        LuaManager.Instance.LuaClient.luaState.DoFile(luaName +".lua");
        LuaManager.Instance.LuaClient.CallFunc(luaName + ".Start", this.gameObject);
    }
}
