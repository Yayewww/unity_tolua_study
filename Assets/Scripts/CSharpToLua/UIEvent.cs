using LuaInterface;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIEvent : MonoBehaviour
{
    public static void AddButtonOnClick(GameObject obj, LuaFunction function)
    {
        if (obj == null)
        {
            Debug.LogError("Lua AddButtonOnClick() obj is NULL");
            return;
        }
        Button btn = obj.GetComponent<Button>();
        btn.onClick.AddListener(
            delegate ()
            {
                function.Call(obj);
            }
        );
    }

    public static void OnClickToLoadScene(string sceneName)
    {
        Debug.Log($"Lua OnClickToLoadScene({sceneName})");
        SceneManager.LoadScene(sceneName);
    }
}
