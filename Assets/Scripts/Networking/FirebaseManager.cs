using Firebase;
using Firebase.Storage;
using Firebase.Auth;
using Google;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

public class FirebaseManager : MonoBehaviour
{
    // Variable
    public bool AutoLogin = true;
    // Event
    public UnityEvent<bool> OnGoogleSignInSuccess = new UnityEvent<bool>();
    public UnityEvent<Texture2D> OnImageDownloaded = new UnityEvent<Texture2D>();

    private FirebaseApp app;
    private FirebaseStorage storage;
    private FirebaseAuth auth;
    private GoogleSignInConfiguration configuration;
    
    // Property
    public static FirebaseManager Instance
    {
        get {return _instance;}
    }
    private static FirebaseManager _instance;
    public FirebaseApp FirebaseApp
    {
        get
        {
            return app;
        }
    }
    public FirebaseStorage FirebaseStorage
    {
        get
        {
            return storage;
        }
    }
    public FirebaseAuth FirebaseAuth
    {
        get
        {
            return auth;
        }
    }
    string strToken;
    // Mono Function
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        _instance = this;
    }

    // Public Function
    public async void UploadImage(Texture2D img)
    {
        var fileName = $"{Guid.NewGuid()}.png";
        var imgReference = storage.GetReference($"/uploadedImage/{fileName}");
        var bytes = img.EncodeToPNG();
        var uploadTask = imgReference.PutBytesAsync(bytes);
        Debug.Log($"Uploading Image : \n {fileName}");
        var metaData = await uploadTask;
        if (uploadTask.Exception != null)
        {
            Debug.Log($"Failed to upload because {uploadTask.Exception}");
            return;
        }
        else
        {
            Debug.Log("Uploading Img Compeleted");
        }
    }
    public async void DownloadImage(string location)//Path in storage ex. "/uploadedImage/filename.jpg"
    {
        var imgReference = storage.GetReference(location);
        var downloadTask = imgReference.GetBytesAsync(long.MaxValue);
        Debug.Log("Downloading Image From : \n" + location);
        var bytes = await downloadTask;
        if (downloadTask.Exception != null)
        {
            Debug.Log($"Failed to download because {downloadTask.Exception}");
            return;
        }
        else
        {
            Debug.Log("Download Compeleted");
        }

        var texture = new Texture2D(2, 2);
        texture.LoadImage(bytes);
        OnImageDownloaded.Invoke(texture);//give out texture
    }
    public void SignInGoogle(){ OnSignIn(); }
    public void SignOutGoogle(){ OnSignOut(); }
    public void SignInSilently() { OnSignInSilently(); }
    public async Task InitFirebase()
    {
        GameManager.Instance.ShowGameStatus("Start Init Firebase");
        var clientId = GlobalConfig.GOOGLE_AUTH_CLIENT_ID;
        configuration = new GoogleSignInConfiguration { WebClientId = clientId, RequestEmail = true, RequestIdToken = true }; //FirebaseAuth
        var dependencyStatus = await FirebaseApp.CheckAndFixDependenciesAsync(); //檢查Firebase是否啟動正確
        if (dependencyStatus == DependencyStatus.Available)
        {
            GlobalConfig.IS_FIREBASE_AVAILABLE = true;

            app = FirebaseApp.DefaultInstance;
            auth = FirebaseAuth.DefaultInstance;
            storage = FirebaseStorage.DefaultInstance;
            Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;
            Firebase.Messaging.FirebaseMessaging.MessageReceived += OnMessageReceived;

            GameManager.Instance.ShowGameStatus("Firebase Available");
            if (AutoLogin)
            {
                SignInSilently();
            }
        }
        else
        {
            GlobalConfig.IS_FIREBASE_AVAILABLE = false;
            GameManager.Instance.ShowGameStatus(System.String.Format("Could not resolve all Firebase dependencies: {0}", dependencyStatus));
        }
        GlobalConfig.IS_FIREBASE_INIT = true;
        GameManager.Instance.ShowGameStatus("Firebase Initialized");
    }
    public void CopyToke()
    {
        Debug.Log("Copy\n" + strToken);
    }
    void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token)
    {
        Debug.Log("Received Registration Token: " + token.Token);
        strToken = token.Token;
    }
    void OnMessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e)
    {
        try
        {
            Debug.Log("Received a new message from: " + e.Message.From);
            Firebase.Messaging.FirebaseMessage message = e.Message;
            var notifi = message.Notification;
            Debug.Log("---------------FirebaseMessage-----------------");

            foreach (var item in message?.Data)
            {
                Debug.Log(item.Key);
                Debug.Log(item.Value);
            }
            Debug.Log("--------------Notification------------------");
            Debug.Log(notifi.Title);
            Debug.Log(notifi.Body);
            Debug.Log(notifi.Icon);
            Debug.Log(notifi.Sound);
            Debug.Log(notifi.Badge);
            Debug.Log(notifi.Color);
            Debug.Log(notifi.ClickAction);

        }
        catch (Exception ex)
        {
            Debug.Log("EXCEPTION - Firebase Message Notification: " + ex.ToString());
        }
    }

    // Member Function
#region Authentication
    private void OnSignIn()
    {
        GoogleSignIn.Configuration = configuration;
        GoogleSignIn.Configuration.UseGameSignIn = false;
        GoogleSignIn.Configuration.RequestIdToken = true;
        GameManager.Instance.ShowGameStatus("Calling SignIn");
        GoogleSignIn.DefaultInstance.SignIn().ContinueWith(OnAuthenticationFinished);
    }
    private void OnSignOut()
    {
        GameManager.Instance.ShowGameStatus("Calling SignOut");
        GoogleSignIn.DefaultInstance.SignOut();
    }
    private void OnDisconnect()
    {
        GameManager.Instance.ShowGameStatus("Calling Disconnect");
        GoogleSignIn.DefaultInstance.Disconnect();
    }
    private void OnSignInSilently()
    {
        GoogleSignIn.Configuration = configuration;
        GoogleSignIn.Configuration.UseGameSignIn = false;
        GoogleSignIn.Configuration.RequestIdToken = true;
        GameManager.Instance.ShowGameStatus("Calling SignIn Silently");

        GoogleSignIn.DefaultInstance.SignInSilently().ContinueWith(OnAuthenticationFinished);
    }
    private void OnGamesSignIn()
    {
        GoogleSignIn.Configuration = configuration;
        GoogleSignIn.Configuration.UseGameSignIn = true;
        GoogleSignIn.Configuration.RequestIdToken = false;
        GameManager.Instance.ShowGameStatus("Calling Games SignIn");

        GoogleSignIn.DefaultInstance.SignIn().ContinueWith(OnAuthenticationFinished);
    }
    internal async void OnAuthenticationFinished(Task<GoogleSignInUser> task)
    {
        if (task.IsFaulted)
        {
            using (IEnumerator<Exception> enumerator = task.Exception.InnerExceptions.GetEnumerator())
            {
                if (enumerator.MoveNext())
                {
                    GoogleSignIn.SignInException error = (GoogleSignIn.SignInException)enumerator.Current;
                    GameManager.Instance.ShowGameStatus("Got Error: " + error.Status + " " + error.Message);
                    OnGoogleSignInSuccess.Invoke(false);
                }
                else
                {
                    GameManager.Instance.ShowGameStatus("Got Unexpected Exception?!?" + task.Exception);
                    OnGoogleSignInSuccess.Invoke(false);
                }
            }
        }
        else if(task.IsCanceled)
        {
            GameManager.Instance.ShowGameStatus("Sign In Canceled");
            OnGoogleSignInSuccess.Invoke(false);
        }
        else
        {
            await new WaitForEndOfFrame();
            string welcomMsg = $"{task.Result.Email}\n歡迎 {task.Result.DisplayName}";
            Debug.Log(task.Result.Email);
            Debug.Log(task.Result.DisplayName);
            Debug.Log(task.Result.IdToken);
            SignInWithGoogleOnFirebase(task.Result.IdToken, welcomMsg);
        }
    }
    private async void SignInWithGoogleOnFirebase(string idToken, string msg)
    {
        Credential credential = GoogleAuthProvider.GetCredential(idToken, null);

        await auth.SignInWithCredentialAsync(credential).ContinueWith(task =>
        {
            AggregateException ex = task.Exception;
            if (ex != null)
            {
                if (ex.InnerExceptions[0] is FirebaseException inner && (inner.ErrorCode != 0))
                {
                    msg = ("Error code = " + inner.ErrorCode + " Message = " + inner.Message);
                    OnGoogleSignInSuccess.Invoke(false);
                }
            }
            else
            {
                OnGoogleSignInSuccess.Invoke(true);
            }
        });
        await new WaitForEndOfFrame();
        GameManager.Instance.ShowGameStatus($"{msg}");
    }
#endregion
}
