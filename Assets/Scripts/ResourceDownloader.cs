using System.Collections;
using System.Collections.Generic;
using Firebase.Storage;
using UnityEngine;
using UnityEngine.Events;

public class ResourceDownloader : MonoBehaviour
{
    public ScreenshotEvent OnScreenshotDownloaded = new ScreenshotEvent();
    public bool bFromLocation = false;
    public void Trigger(string url)
    {
        if (bFromLocation || url == null || url == "")
        {
            Debug.Log($"Download Image From : '/screenshots/AAA_2.png'");
            StartCoroutine(DownloadScreenshotCoroutineByLocation());
        }
        else
        {
            Debug.Log($"Download Image From Url : \n {url}");
            StartCoroutine(DownloadScreenshotCoroutine(url));
        }
    }

    IEnumerator DownloadScreenshotCoroutineByLocation()
    {
        var storage = FirebaseStorage.DefaultInstance;
        var screenshotRef = storage.GetReference($"/screenshots/AAA_2.png");
        var downloadTask = screenshotRef.GetBytesAsync(long.MaxValue);
        yield return new WaitUntil(() => downloadTask.IsCompleted);
        Debug.Log("Download finished By Location");

        var texture = new Texture2D(2, 2);
        texture.LoadImage(downloadTask.Result);
        OnScreenshotDownloaded.Invoke(texture);
    }

    IEnumerator DownloadScreenshotCoroutine(string url)
    {
        var storage = FirebaseStorage.DefaultInstance;
        var rootPath = storage.RootReference.Path;
        var screenshotRef = storage.GetReferenceFromUrl(url);//這個要用gs://
        var downloadTask = screenshotRef.GetBytesAsync(long.MaxValue);
        yield return new WaitUntil(() => downloadTask.IsCompleted);
        Debug.Log($"Download finished from \n : {url}");

        var texture = new Texture2D(2, 2);
        texture.LoadImage(downloadTask.Result);
        OnScreenshotDownloaded.Invoke(texture);
    }

    [System.Serializable]
    public class ScreenshotEvent : UnityEvent<Texture2D>
    {

    }
}
