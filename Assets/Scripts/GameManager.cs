using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    //Variable
    Canvas canvas;
    Text text_status;
    Text text_version;
    Transform panel_loading;
    Transform panel_finishload;

    // Property
    public static GameManager Instance
    {
        get {return _instance;}
    }
    private static GameManager _instance;

    //Mono Function
    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        Application.targetFrameRate = 60;
        _instance = this;
        SceneManager.sceneLoaded += OnSceneLoaded;
        //Children
        canvas = gameObject.GetComponentInChildren<Canvas>();
        text_status = canvas.transform.Find("Text_Status").GetComponent<Text>();
        text_version = canvas.transform.Find("Text_Version").GetComponent<Text>();
        panel_loading = canvas.transform.Find("Panel_Loading");
        panel_finishload = canvas.transform.Find("Panel_FinishLoad");
        //全關
        text_status.gameObject.SetActive(false);
        text_version.gameObject.SetActive(false);
        panel_loading.gameObject.SetActive(false);
        panel_finishload.gameObject.SetActive(false);
    }
    async void Start()
    {
        await FirebaseManager.Instance.InitFirebase();
        await AssetbundleManager.Instance.CheckNewVersion();
        text_version.text = "當前版本 : " + AssetbundleManager.Instance.GetCurrentVersion();
        SceneManager.LoadScene("Update");
    }
    // Public Function
    public void ShowGameStatus(string status)
    {
        Debug.Log(status);
        text_status.gameObject.SetActive(true);
        text_status.text = status;
    }
    public void ReadyToStartGame()
    {
        ShowGameStatus("點擊中心");
        ShowLoading(false);
        ShowEnterCursor(true);
    }
    public void ShowLoading(bool bshow)
    {
        if (bshow)
        {
            panel_loading.gameObject.SetActive(true);
            panel_loading.GetComponent<Animation>().Play();
        }
        else
        {
            panel_loading.GetComponent<Animation>().Stop();
            panel_loading.gameObject.SetActive(false);
        }
    }

    public void ShowEnterCursor(bool bShow)
    {
        if (bShow)
        {
            panel_finishload.gameObject.SetActive(true);
            panel_finishload.GetComponent<Animation>().Play();
        }
        else
        {
            panel_finishload.GetComponent<Animation>().Stop();
            panel_finishload.gameObject.SetActive(false);
        }
    }

    public void GoScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
    public void OnGoogleSignIn()
    {
        
    }

    // Member Function
    void OnSceneLoaded(Scene scene, LoadSceneMode loadMode)
    {
        Debug.Log("GameManager.OnSceneLoaded() name = " + scene.name);
        if (scene.name == "Update")
        {
            StartCoroutine(OnUpdateSceneLoaded());
        }
        else if(scene.name == "Lobby")
        {
            Debug.Log("成功來到大廳");
            StartCoroutine(OnLobbySceneLoaded());
        }
        else if(scene.name == "Game")
        {
            Debug.Log("成功進入遊戲");
            text_version.gameObject.SetActive(false);
            text_status.gameObject.SetActive(false);
        }
    }

    IEnumerator OnUpdateSceneLoaded()
    {
        ShowLoading(true);
        ShowEnterCursor(false);
        text_version.gameObject.SetActive(true);
        yield return new WaitForSeconds(1.0f);
        //Loading
        AssetbundleManager.Instance.InitBundles();
    }

    IEnumerator OnLobbySceneLoaded()
    {
        ShowEnterCursor(false);
        text_version.gameObject.SetActive(true);
        text_status.gameObject.SetActive(false);
        yield return new WaitForSeconds(1.0f);
    }
}

