using UnityEngine;

// Variable
// Property
// Event
// Mono Function
// Public Function
// Coroutine
// Member Function

public static class GlobalConfig
{
    public static bool IS_FIREBASE_INIT = false;
    public static bool IS_FIREBASE_AVAILABLE = false;
    public static string GOOGLE_AUTH_CLIENT_ID = "777261835716-i7oeg543bbkbensmafcnh55mb4t7c81c.apps.googleusercontent.com";
    public static string FIREBASE_STORAGE_PATCH_URL = @"https://storage.googleapis.com/tolua-study.appspot.com/patch";
    public static string FIREBASE_STORAGE_BUNDLEDATA_URL = @"https://storage.googleapis.com/tolua-study.appspot.com/patch/BundleData/BundleData.json";
    public static string LOCAL_BUNDLEDATA_PATH = Application.persistentDataPath + "/BundleData.json";
    public static string LOCAL_SAVE_DATA_PATH = Application.persistentDataPath + "/user_save_data.txt";
}

