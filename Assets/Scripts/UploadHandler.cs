using System;
using System.Collections;
using System.Collections.Generic;
using Firebase.Storage;
using UnityEngine;

public class UploadHandler : MonoBehaviour
{
    public ResourceDownloader resourceDownloader;

    public void StartUpload(Texture2D screenshot)
    {
        StartCoroutine(UploadCoroutine(screenshot));
    }

    IEnumerator UploadCoroutine(Texture2D screenshot)
    {
        var storage = FirebaseStorage.DefaultInstance;
        var screenshotRef = storage.GetReference($"/screenshots/AAA_2.png");
        var bytes = screenshot.EncodeToPNG();
        var uploadTask = screenshotRef.PutBytesAsync(bytes);
        Debug.Log("Start Upload");
        yield return new WaitUntil(() => uploadTask.IsCompleted);
        if (uploadTask.Exception != null)
        {
            Debug.Log($"Failed to upload because {uploadTask.Exception}");
            yield break;
        }

        Debug.Log("Upload Success, Get Url Now.");
        var getUrlTask = screenshotRef.GetDownloadUrlAsync();
        yield return new WaitUntil(() => getUrlTask.IsCompleted);
        if (getUrlTask.Exception != null)
        {
            Debug.Log($"Failed to get a download url with {getUrlTask.Exception}");
            yield break;
        }
        Debug.Log($"Start Download from \n {getUrlTask.Result}");
        var url = getUrlTask.Result.ToString();
        resourceDownloader.Trigger("");
    }
}
