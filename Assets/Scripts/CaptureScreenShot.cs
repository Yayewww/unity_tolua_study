using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class CaptureScreenShot : MonoBehaviour, IPointerClickHandler
{
    public UnityEvent<Texture2D> OnScreenCaptured = new UnityEvent<Texture2D>();

    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log("Call Capture");
        StartCoroutine(CaptureScreenshotCoroutine());
    }

    private IEnumerator CaptureScreenshotCoroutine()
    {
        yield return new WaitForEndOfFrame();
        var screenshot = ScreenCapture.CaptureScreenshotAsTexture();
        OnScreenCaptured.Invoke(screenshot);
    }
}
