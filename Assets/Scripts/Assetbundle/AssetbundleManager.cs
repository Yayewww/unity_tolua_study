using System;
using System.IO;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using LuaInterface;

public class AssetbundleManager : MonoBehaviour
{
    // Public Variable
    public static AssetbundleManager Instance
    {
        get {return _instance;}
    }
    private static AssetbundleManager _instance;

    [Header("發布前請確認下方參數")]
    public bool useLocal = false;
    public GameObject luaManagerReference;
    // Private Variable
    int bundleCount = 0;
    List<string> bundleNameList;
    BundleData newBundleData;
    BundleData curBundleData;
    LuaFileUtils fileUtils;
    GameObject luaManager;
    AssetBundle sceneBundle;
    List<AssetBundle> luaBundles = new List<AssetBundle>();

    // Mono Function
    private void Awake() 
    {
        DontDestroyOnLoad(gameObject);
        _instance = this;
    }

    // Public Funciton
    public async Task<AssetBundle> LoadBundleByName(string name)//要輸入含有副檔名的全名
    {
        if (bundleNameList == null)
        {
            return null;
        }
        var index = bundleNameList.IndexOf(name);
        if (index != -1)
        {
            string bundleName = bundleNameList[index];//含有副檔名
            string bundlePath = GetBundlePath(bundleName);
            string fileName = Path.GetFileNameWithoutExtension(bundleName);//不含副檔名
            Hash128 hash = GetHashFromBundleData(bundleName);
            var request = UnityWebRequestAssetBundle.GetAssetBundle(bundlePath, hash);
            await request.SendWebRequest();
            var bundle = DownloadHandlerAssetBundle.GetContent(request);
            return bundle;
        }
        return null;
    }
    public async Task<bool> CheckNewVersion()//版本檢測會更新bundleData
    {        
        GameManager.Instance.ShowGameStatus($"取得當前版本......");
        //先取一次當前的
        if (curBundleData != null)
        {
            //有Data, 不做事.
        }
        else
        {
            //沒Data, 弄個空的.
            curBundleData = new BundleData();
            curBundleData.Names = new List<string>();
            curBundleData.Hashes = new List<string>();
            curBundleData.nativeVersion = "0.0.0";
        }
        GameManager.Instance.ShowGameStatus($"Current Version: [{curBundleData.nativeVersion}]");
        //取一次最新的
        GameManager.Instance.ShowGameStatus($"取得最新版本......");
        newBundleData = null;
        
        var bundlePath = "";
        if (useLocal)
        {
            bundlePath = GlobalConfig.LOCAL_BUNDLEDATA_PATH;
        }
        else
        {
            bundlePath = GlobalConfig.FIREBASE_STORAGE_BUNDLEDATA_URL;
        }

        var request = UnityWebRequest.Get(bundlePath);
        var loader = new DownloadHandlerBuffer();
        request.downloadHandler = loader;
        await request.SendWebRequest();       
        switch (request.result)
        {
        case UnityWebRequest.Result.ConnectionError:
        case UnityWebRequest.Result.DataProcessingError:
            Debug.LogError(": Error: " + request.error);
            break;
        case UnityWebRequest.Result.ProtocolError:
            Debug.LogError(": HTTP Error: " + request.error);
            break;
        case UnityWebRequest.Result.Success:
            GameManager.Instance.ShowGameStatus("NewBundleData Download Complete.");
            var fileContents = loader.text;//載出來的是json字串
            newBundleData = JsonUtility.FromJson<BundleData>(fileContents);
            GameManager.Instance.ShowGameStatus($"New Version: [{newBundleData.nativeVersion}]");
            break;
        }
        //版本檢測結果
        if (newBundleData == null)
        {
            return false;
        }
        else if (newBundleData.nativeVersion == curBundleData.nativeVersion)
        {
            return false;
        }
        else
        {
            curBundleData = newBundleData;
            return true;
        }
    }

    public string GetCurrentVersion()
    {
        var ver = "1.0.0";
        if (curBundleData != null)
        {
            ver = curBundleData.nativeVersion;
        }
        return ver;
    }

    public void InitBundles()
    {
        Debug.Log("Init Bundles");
        //重載前Reset
        ResetBundles();
        fileUtils = new LuaFileUtils();//他會負責把Lua寫進buffer
        if (useLocal)
        {
            Debug.Log("跳過Init Bundle");
            //改false可以用回讀Assets/Lua跟Assets/ToLua/Lua底下的Lua
            //要編輯器測試不包Bundle就可以跳過讀Bundle的部分, 然後把Scene加到BuildSettings
            fileUtils.beZip = false;
            OnBundleLoaded();
        }
        else
        {
            fileUtils.beZip = true; //true的意義在於這是從bunlde讀取
            StartCoroutine(LoadAllBundles());
        }
    }

    // Member Funciton
    void ResetBundles()
    {
        //釋放順序 1. LuaFileUtils 跟 LuaManager, 2.手動紀錄的Bundle
        Debug.Log("釋放一波");
        if (luaManager != null)
        {
            GameObject.Destroy(luaManager);
            luaManager = null;
        }

        if (fileUtils != null)
        {
            fileUtils.Dispose();//LuaFileUtils 在 Dispose() 的時候會連同 AddSearchBundle過的Bundle一起Unload(true).
            fileUtils = null;
        }
        //之所以補了一個手動清luaBundles的原因是不知道為什麼fileUtils在第三次Reload的時候instance變成null，所以Dispose()失效。
        //沒時間查，所以弄個手動清的實現。
        if (luaBundles != null)
        {
            foreach (var bundle in luaBundles)
            {
                if (bundle != null)
                {
                    bundle.Unload(true);
                }
            }
            luaBundles = new List<AssetBundle>();
        }
        //釋放Scene
        if (sceneBundle != null)
        {
            sceneBundle.Unload(true);
            sceneBundle = null;
        }
    }
    IEnumerator LoadAllBundles()
    {
        //每次都要載最新的Main bundle.
        string mainBundlePath = GetBundlePath(LuaConst.osDir);
        Debug.Log($"Start Getting MainBundle");
        var request = UnityWebRequestAssetBundle.GetAssetBundle(mainBundlePath);//不加Hash
        yield return request.SendWebRequest();
        
        switch (request.result)
        {
            case UnityWebRequest.Result.Success:
                {
                    Debug.Log("Success to get Newest MainBundle.");
                }
                break;
            case UnityWebRequest.Result.ConnectionError:
                {
                    Debug.LogError("ConnectionError.\n the request couldn't connect or it could not establish a secure channel.");
                }
                break;
            case UnityWebRequest.Result.DataProcessingError:
                {
                    Debug.LogError("DataProcessingError.\n The request succeeded in communicating with the server, but received an error as defined by the connection protocol.");
                }
                break;
            case UnityWebRequest.Result.ProtocolError:
                {
                    Debug.LogError("ProtocolError.\n The request succeeded in communicating with the server, but encountered an error when processing the received data.");
                }
                break;
            default:
                break;
        }
        
        var mainBundle = DownloadHandlerAssetBundle.GetContent(request);
        request.Dispose();

        var manifestRequest = mainBundle.LoadAssetAsync<AssetBundleManifest>("AssetBundleManifest");
        yield return manifestRequest;

        AssetBundleManifest manifest = manifestRequest.asset as AssetBundleManifest;
        bundleNameList = new List<string>(manifest.GetAllAssetBundles());//不包含Mainbundle自己
        bundleCount = bundleNameList.Count;
        Debug.Log($"Total Bundle Count {bundleCount}");

        for (int i = 0; i < bundleNameList.Count; i++)
        {
            string bundleName = bundleNameList[i];//含有副檔名
            string bundlePath = GetBundlePath(bundleName);
            string fileName = Path.GetFileNameWithoutExtension(bundleName);//不含副檔名
            Hash128 hash = GetHashFromBundleData(bundleName);
            StartCoroutine(LoadBundle(fileName, bundlePath, hash));
        }

        mainBundle.Unload(true);
        yield return StartCoroutine(WaitLoading());
    }
    IEnumerator LoadBundle(string fileName, string bundlePath, Hash128 hash)
    {
        if (Caching.IsVersionCached(bundlePath, hash))
        {
            Debug.Log($"[{fileName}]用快取");
        }
        else
        {
            Debug.Log($"[{fileName}]無快取");
        }

        using(var request = UnityWebRequestAssetBundle.GetAssetBundle(bundlePath, hash))
        {
            yield return request.SendWebRequest();
            switch (request.responseCode)
            {
                case 0://用快取
                {
                    break;
                }
                case 200://用下載
                {
                    var contentLength = request.GetRequestHeader("CONTENT-LENGTH");
                    long.TryParse(contentLength, out long fileSize);
                    Debug.Log($"{fileName} 下載完成, 大小:{fileSize}");
                    break;
                }
                default:
                {
                    request.Dispose();
                    Debug.LogError($"[{fileName}]REQUEST ERROR {request.responseCode}\n{request.error}");
                    yield break;
                }
            }

            AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(request);

            //載入Lua的分水嶺
            if (fileName.StartsWith("lua"))
            {
                LuaFileUtils.Instance.AddSearchBundle(fileName, bundle);//luaBundle不可以手動Unload, 請用LuaFileUtils.Dispose()
                luaBundles.Add(bundle);
            }
            else if (fileName.StartsWith("scene"))
            {
                sceneBundle = bundle;
            }
            else
            {
                //非lua的Unload，釋放記憶體資源。
                bundle.Unload(true);
            }

            request.Dispose();
            --bundleCount;
        }
    }

    IEnumerator WaitLoading()
    {
        while (bundleCount > 0)
        {
            yield return null;
        }
        yield return new WaitForSeconds(1.0f);
        Debug.Log("Load All Bundles Complete!!");
        OnBundleLoaded();
    }

    void OnBundleLoaded()
    {
        //全部取完最新的bundle後，會啟動Main.lua
        luaManager = GameObject.Instantiate<GameObject>(luaManagerReference);
        GameManager.Instance.ReadyToStartGame();
    }

    string GetBundlePath(string bundleName)
    {
        string bundlePath = "";
        if(GlobalConfig.IS_FIREBASE_AVAILABLE && useLocal == false)
        {
            bundlePath = GlobalConfig.FIREBASE_STORAGE_PATCH_URL + "/" + LuaConst.osDir + "/" + bundleName;
        }
        else
        {
            bundlePath = Application.streamingAssetsPath + "/" + LuaConst.osDir + "/" + bundleName;
        }
        return bundlePath;
    }

    Hash128 GetHashFromBundleData(string bundleName)
    {
        var index = curBundleData.Names.IndexOf(bundleName);//找不到會回傳-1
        if (index > (curBundleData.Hashes.Count - 1) || index == -1)
        {
            return Hash128.Compute("");
        }
        var strHash = curBundleData.Hashes[index];
        return Hash128.Compute(strHash);
    }
}
