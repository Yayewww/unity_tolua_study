using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectManager : MonoBehaviour
{
    [Header("Prefab")]
    public GameObject[] prefabs;
    [Header("In Game")]
    public GameObject[] objects;
    public Dictionary<string, GameObject> ObjectMap;
    public Dictionary<string, GameObject> PrefabMap;

    void Awake() 
    {
        InitObjectMap();
        InitPrefabMap();
    }
    void InitPrefabMap()
    {
        Debug.Log("Init PrefabMap");
        PrefabMap = new Dictionary<string, GameObject>();
        if (prefabs.Length <= 0)
        {
            Debug.Log("!!!!!There's no prefab registy in ObjectManager!!!!!");
        }
        for (int i = 0; i < prefabs.Length; i++)
        {
            var name = prefabs[i].name;
            var obj = prefabs[i];
            PrefabMap.Add(name, obj);
        }
    }
    void InitObjectMap()
    {
        Debug.Log("Init ObjectMap");
        ObjectMap = new Dictionary<string, GameObject>();
        if (objects.Length <= 0)
        {
            Debug.Log("!!!!!There's no object registy in ObjectManager!!!!!");
        }
        for (int i = 0; i < objects.Length; i++)
        {
            var name = objects[i].name;
            var obj = objects[i];
            ObjectMap.Add(name, obj);
        }
    }
        public GameObject GetPrefabFromMap(string key)
    {
        if (PrefabMap.ContainsKey(key))
        {
            var obj = PrefabMap[key];
            return obj;
        }
        Debug.LogError($"Try to get [Prefab] [{key}] IS NULL");
        return null;
    }

    public GameObject GetObjectFromMap(string key)
    {
        if (ObjectMap.ContainsKey(key))
        {
            var obj = ObjectMap[key];
            return obj;
        }
        Debug.LogError($"Try to get [GameObject] [{key}] IS NULL");
        return null;
    }
}
