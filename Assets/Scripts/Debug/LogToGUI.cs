using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class LogToGUI : MonoBehaviour
{
    [Header("File Read & Write")]
    public bool bAutoWriteToFile = false;
    public bool bWriteToFileWhenExit = false;
    [Header("Stacktrace Option")]
    public bool bRecordAllStack = false;
    public bool bRecordStackByError = false;
    public bool bRecordStackByException = false;
    public bool bRecordStackByWarning = false;
    public bool bRecordStackByAssert = false;
    public bool bRecordStackByLog = false;
    [Header("UI")]
    public Text logText;
    public RectTransform scrollView;
    public int     kChars      = 9999;
    
    string totalLog    = "*[FILE]begin log from";
    string  guiLog      = "*[GUI]begin log from";
    string  dirName     = "LogOutput";
    string  logFilePath = "";
    bool    bDoShow      = true;
    bool    bDone = false;
    int     writeTimes = 0;
    float holdTime = 0;
    DateTime dtLaunch;

    private void Awake()
    {
        // if this is NOT a debug build
        if (!Debug.isDebugBuild)
        {
            Destroy(this);
            return;
        }
        
        DontDestroyOnLoad(gameObject);

        dtLaunch = DateTime.Now;
        totalLog = totalLog + "[" + dtLaunch.ToString("yyyy-MM-dd") + "]";
        guiLog   = guiLog + "[" + dtLaunch.ToString("yyyy-MM-dd") + "]";
        InitLogFilePath();
        Application.logMessageReceived += Log;
        Debug.Log("Log To GUI Awake");
        var OS = GetOS();
        var width = Screen.width;
        var height = Screen.height;
        Debug.Log($"Device Platfform: {OS}");
        Debug.Log($"Device Resolution: {width}x{height}");
    }
    private void Start()
    {
        ShowLogWindow();
    }
    private void OnDisable() {
        Debug.Log("Log To GUI Disable");
        Application.logMessageReceived -= Log; 
    }
    private void Update()
    {
        //Show log window
        if (Input.touchCount >= 1)
        {
            Touch targetFinger = Input.GetTouch(0);
            holdTime += Time.deltaTime;
            if (holdTime > 3.0f && bDone == false)
            {
                bDone = true;
                ShowLogWindow();
            }
        }
        else
        {
            bDone = false;
            holdTime = 0.0f;
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            ShowLogWindow();
        }
    }
    private void OnApplicationPause(bool pause)
    {
        totalLog = totalLog + "\n\n" + "##On Application Pause##";
        if (bWriteToFileWhenExit)
        {
            WriteLogToFile(totalLog, logFilePath);
        }
    }
    private void OnApplicationQuit()
    {
        totalLog = totalLog + "\n\n" + "##On Application Quit##";
        if (bWriteToFileWhenExit)
        {
            WriteLogToFile(totalLog, logFilePath);
        }
    }

    public void Log(string logString, string stackTrace, LogType type)
    {
        var timeStamp = "[" + DateTime.Now.ToLongTimeString() + "]";
        var logContent = timeStamp + logString;

        if (bRecordAllStack)
        {
            logContent = logContent + "\n" + stackTrace;
        }
        else
        {
            switch (type)
            {
                case LogType.Error:
                    if (bRecordStackByError)
                    {
                        logContent = logContent + "\n" + stackTrace;
                    }
                    break;
                case LogType.Assert:
                    if (bRecordStackByAssert)
                    {
                        logContent = logContent + "\n" + stackTrace;
                    }
                    break;
                case LogType.Warning:
                    if (bRecordStackByWarning)
                    {
                        logContent = logContent + "\n" + stackTrace;
                    }
                    break;
                case LogType.Log:
                    if (bRecordStackByLog)
                    {
                        logContent = logContent + "\n" + stackTrace;
                    }
                    break;
                case LogType.Exception:
                    if (bRecordStackByException)
                    {
                        logContent = logContent + "\n" + stackTrace;
                    }
                    break;
                default:
                    break;
            }
        }
        totalLog = totalLog + "\n" + logContent;
        guiLog = totalLog;

        if (guiLog.Length > kChars)
        {
            guiLog = guiLog.Substring(guiLog.Length - kChars);
        }
        logText.text = guiLog;

        if (bAutoWriteToFile)
        {
            WriteLogToFile(totalLog, logFilePath);
        }
    }

    private void InitLogFilePath()
    {
        var logDir = "";
        var platform = Application.platform;
        if (platform == RuntimePlatform.WindowsEditor || platform == RuntimePlatform.WindowsPlayer)
        {
            logDir = Path.Combine(Application.streamingAssetsPath, dirName);
        }
        else if (platform == RuntimePlatform.Android)
        {
            logDir = Path.Combine(Application.persistentDataPath, dirName);
        }

        if (!Directory.Exists(logDir))
        {
            Directory.CreateDirectory(logDir);
        }
        var timesPath = Path.Combine(logDir, "LogTimes" + dtLaunch.ToString("yyyy-MM-dd") + ".txt");

        if (!File.Exists(timesPath))
        {
            StreamWriter sw = new StreamWriter(timesPath);
            sw.Write(writeTimes.ToString());
            sw.Close();
        }
        else
        {
            StreamReader sr = new StreamReader(timesPath);
            writeTimes = int.Parse(sr.ReadLine());
            writeTimes += 1;
            sr.Close();
        }

        FileStream fs = new FileStream(timesPath, FileMode.Create, FileAccess.ReadWrite);
        StreamWriter tsw = new StreamWriter(fs);
        tsw.Write(writeTimes.ToString());
        tsw.Close();

        var fileName = "Log_"+ dtLaunch.ToString("yyyy-MM-dd") + "_" + writeTimes.ToString() + ".txt";
        logFilePath = Path.Combine(logDir, fileName);
    }

    private void WriteLogToFile(string logHistory, string path)
    {
        FileStream fs = new FileStream(path, FileMode.Create, FileAccess.ReadWrite);
        StreamWriter sw = new StreamWriter(fs);
        sw.Write(logHistory);
        sw.Close();
    }

    private void ShowLogWindow()
    {
        if (bDoShow)
        {
            scrollView.gameObject.SetActive(false);
            bDoShow = false;
        }
        else
        {
            scrollView.gameObject.SetActive(true);
            bDoShow = true;
        }
    }

    string GetOS()
    {
        string osDir = "";
#if UNITY_STANDALONE
        osDir = "Win";
#elif UNITY_ANDROID
        osDir = "Android";            
#elif UNITY_IPHONE
        osDir = "iOS";        
#else
        osDir = "Unknow";        
#endif
        return osDir;
    }
}
