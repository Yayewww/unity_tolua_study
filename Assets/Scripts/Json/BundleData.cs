using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class BundleData
{
    public string nativeVersion;
    public List<string> Names;
    public List<string> Hashes;
}
