--Scene的主要作用是來初始化與創建、設置對象，入口統一由C#呼叫
LobbyScene = {}

function LobbyScene.Start(obj)
    print("=====LobbyScene.Start()=====")
    local objectManager = obj:GetComponent(typeof(ObjectManager))
    
    local Btn_Start = objectManager:GetObjectFromMap("Btn_Start")
    G_UIEvent.AddButtonOnClick(Btn_Start, function ()
        G_UIEvent.OnClickToLoadScene("Game")
    end)

    local Btn_GoogleSignIn = objectManager:GetObjectFromMap("Btn_GoogleSignIn")
    G_UIEvent.AddButtonOnClick(Btn_GoogleSignIn, function ()
        G_Util.SignInGoogle()
    end)

    local Text_HighScore = objectManager:GetObjectFromMap("Text_HighScore")
    local saveDataStr = G_Util.ReadScore()
    local scoreStr = "最高紀錄 : "..saveDataStr
    Text_HighScore:GetComponent(typeof(Text)).text = scoreStr
end

