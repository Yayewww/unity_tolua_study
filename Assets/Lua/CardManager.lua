local CardManager = {}
local CardConfig = require("CardConfig")

function CardManager.NewCards()
    math.randomseed(os.time())
    --複製一份
    local copyCards = {}
    for i, v in ipairs(CardConfig) do
        local card = v
        table.insert(copyCards, card)
    end
    --洗牌
    local shuffleCards = {}
    for i = 1, 52, 1 do
        local count = #copyCards
        local index = math.random(1, count)
        local card = copyCards[index]
        table.insert(shuffleCards, card)
        table.remove(copyCards, index)
    end
    return shuffleCards
end

function CardManager.InitUICardsTable(obj_cards)--userdata, transform
    print("init ui cards")
    local ui_cards_table = {}
    for i = 1, 52, 1 do
        local key = obj_cards[i].name --c_suit_num
        ui_cards_table[key] = obj_cards[i]
    end
    return ui_cards_table
end

function CardManager.GetBiggestHandRanking(sevenCards)
    local result = 
    {
        hand_ranking = 0,
        biggestNum = 0,
        hand_name = "high-card",
        isRoyalStraightFlush = false, --皇家同花順
        isStraightFlush = false, --同花順
        isFourOfKind = false, --四條
        isFullHouse = false, --葫蘆
        isFlush = false, --同花
        isStraight = false, --順子
        isThreeOfKind = false, --三條
        isTwoPair = false, --兩對
        isOnePair = false, --一對
        isHighCard = false, --高牌
        combine = sevenCards,
    }
    --由大到小排序, AKQJ10....
    table.sort(sevenCards, function (cardA, cardB)
        return CardManager.IsCardABiggerThanCardB(cardA, cardB)
    end)

    CardManager.IsHaveStraightFlush(sevenCards, result)
    if result.isRoyalStraightFlush then
        result.hand_ranking = 10
        result.biggestNum = 1
        result.hand_name = "Royal-Straight-Flush"
        return result
    elseif result.isStraightFlush then
        result.hand_ranking = 9
        result.biggestNum = result.combine[1].num
        result.hand_name = "Straight-Flush"
        return result
    end
    CardManager.IsHaveFourOfKind(sevenCards, result)
    if result.isFourOfKind then
        result.hand_ranking = 8
        result.biggestNum = result.combine[5].num
        result.hand_name = "Four-Of-A-Kind"
        return result
    end
    CardManager.IsHaveFullHouse(sevenCards, result)
    if result.isFullHouse then
        result.hand_ranking = 7
        result.biggestNum = result.combine[1].num
        result.hand_name = "Full-House"
        return result
    end
    CardManager.IsHaveFlush(sevenCards, result)
    if result.isFlush then
        --這邊濾掉多的牌，同花有可能>=5張
        local filting_cards = {}
        for i = 1, 5, 1 do
            table.insert(filting_cards, result.combine[i])
        end
        result.combine = filting_cards
        result.hand_ranking = 6
        result.biggestNum = result.combine[1].num
        result.hand_name = "Flush"
        return result
    end
    CardManager.IsHaveStraight(sevenCards, result)
    if result.isStraight then
        result.hand_ranking = 5
        result.biggestNum = result.combine[1].num
        result.hand_name = "Straight"
        return result
    end
    CardManager.IsHaveThreeOfKind(sevenCards, result)
    if result.isThreeOfKind then
        result.hand_ranking = 4
        result.biggestNum = result.combine[4].num
        result.hand_name = "Three-Of-A-Kind"
        return result
    end
    CardManager.IsHaveTwoPair(sevenCards, result)
    if result.isTwoPair then
        result.hand_ranking = 3
        result.biggestNum = result.combine[5].num
        result.hand_name = "Two-Pair"
        return result
    end
    CardManager.IsHaveOnePair(sevenCards, result)
    if result.isOnePair then
        result.hand_ranking = 2
        result.biggestNum = result.combine[3].num
        result.hand_name = "One-Pair"
        return result
    end
    --剩下只能是HighCard了
    result.isHighCard = true
    result.hand_ranking = 1
    result.biggestNum = sevenCards[1].num
    result.hand_name = "High-Card"
    local highCardCombine = 
    {
        sevenCards[1],
        sevenCards[2],
        sevenCards[3],
        sevenCards[4],
        sevenCards[5],
    }
    result.combine = highCardCombine

    return result
end
--測同花順
function CardManager.IsHaveStraightFlush(sevenCards, result)
    local isStraightResult = {}
    local isFlushResult = CardManager.IsHaveFlush(sevenCards, result)
    --有花, 用同花組合(可能5~7張)去找是不是順子
    if isFlushResult.isFlush == true then
        isStraightResult = CardManager.IsHaveStraight(isFlushResult.combine, result)

        if isStraightResult.isStraight == true then
            result.isStraightFlush = true
            result.combine = isStraightResult.combine
            --有同花順, 看是不是皇家同花順
            if result.combine[1].num == 1 and result.combine[2].num == 13 then
                if result.combine[3].num == 12 and result.combine[4].num == 11 then
                    if result.combine[5].num == 10 then
                        result.isRoyalStraightFlush = true
                    end
                end
            end
        end
    end

    return result
end
--測四條
function CardManager.IsHaveFourOfKind(sevenCards, result)
    --先複製一份副本
    local copyCards = {}
    for i, card in ipairs(sevenCards) do
        table.insert(copyCards, sevenCards[i])
    end

    local fourOfKindCards = {}
    --測四張就好
    for i = 1, 4, 1 do
        if sevenCards[i].num == sevenCards[i+1].num then
            if sevenCards[i+1].num == sevenCards[i+2].num then
                if sevenCards[i+2].num == sevenCards[i+3].num then
                    --選中的四張
                    table.insert(fourOfKindCards, sevenCards[i])
                    table.insert(fourOfKindCards, sevenCards[i+1])
                    table.insert(fourOfKindCards, sevenCards[i+2])
                    table.insert(fourOfKindCards, sevenCards[i+3])
                    --從副本移除(要從後面刪除)
                    table.remove(copyCards, i+3)
                    table.remove(copyCards, i+2)
                    table.remove(copyCards, i+1)
                    table.remove(copyCards, i)
                    --從副本選出最大的一張
                    table.insert(fourOfKindCards, copyCards[1])

                    result.isFourOfKind = true
                    result.combine = fourOfKindCards
                    break --有結果就打破迴圈
                end
            end
        end
    end
    
    return result
end
--測葫蘆
function CardManager.IsHaveFullHouse(sevenCards, result)
    --先複製一份副本
    local copyCards = {}
    for i, card in ipairs(sevenCards) do
        table.insert(copyCards, sevenCards[i])
    end
    
    local fullHouseCards = {}
    --已排序過, 用前面五張找三條
    for i = 1, 5, 1 do
        if (sevenCards[i].num == sevenCards[i+1].num) and
            (sevenCards[i].num == sevenCards[i+2].num) then
            table.insert(fullHouseCards, sevenCards[i])
            table.insert(fullHouseCards, sevenCards[i+1])
            table.insert(fullHouseCards, sevenCards[i+2])
            --從副本移除(要從後面刪除)
            table.remove(copyCards, i+2)
            table.remove(copyCards, i+1)
            table.remove(copyCards, i)
            --找到三條, 剩下的找對子
            for j = 1, 3, 1 do
                if (copyCards[j].num == copyCards[j+1].num) then
                    table.insert(fullHouseCards, copyCards[j])
                    table.insert(fullHouseCards, copyCards[j+1])
                    result.isFullHouse = true
                    result.combine = fullHouseCards
                    break --有結果就打破迴圈
                end
            end
            break --找到三條還找不到對子, 也不用找了
        end
    end

    return result
end
--測同花
function CardManager.IsHaveFlush(sevenCards, result)
    local spadeCards = {}
    local heartCards = {}
    local diamondCards = {}
    local clubCards = {}

    for i, card in ipairs(sevenCards) do
        local suit = card.suit
        if suit == "spade" then
            table.insert(spadeCards, card)
        elseif suit == "heart" then
            table.insert(heartCards, card)
        elseif suit == "diamond" then
            table.insert(diamondCards, card)
        elseif suit == "club" then
            table.insert(clubCards, card)
        end
    end

    if #spadeCards >= 5 then
        result.isFlush = true
        result.combine = spadeCards
    elseif #heartCards >= 5 then
        result.isFlush = true
        result.combine = heartCards
    elseif #diamondCards >= 5 then
        result.isFlush = true
        result.combine = diamondCards
    elseif #clubCards >= 5 then
        result.isFlush = true
        result.combine = clubCards
    end

    return result
end
--測順子, 不一定有7張, 最少5張
function CardManager.IsHaveStraight(cards, result)
    local straightCards = {}
    --有七張只看前面三張, 有六張只看前面兩張...依此類推
    for i = 1, (#cards - 4), 1 do
        --先處理AKQJ10的情況
        if cards[i].num == 1 and cards[i+1].num == 13 then
            if cards[i+2].num == 12 and cards[i+3].num == 11 then
               if cards[i+4].num == 10 then
                    table.insert(straightCards, cards[i])
                    table.insert(straightCards, cards[i+1])
                    table.insert(straightCards, cards[i+2])
                    table.insert(straightCards, cards[i+3])
                    table.insert(straightCards, cards[i+4])
                    result.isStraight = true
                    result.combine = straightCards
                    break --有結果就打破迴圈
               end
            end
        --一般情況 ex. 54321
        elseif (cards[i].num - 1) == cards[i+1].num then
            if (cards[i+1].num - 1) == cards[i+2].num then
                if (cards[i+2].num - 1) == cards[i+3].num then
                    if (cards[i+3].num - 1) == cards[i+4].num then
                        table.insert(straightCards, cards[i])
                        table.insert(straightCards, cards[i+1])
                        table.insert(straightCards, cards[i+2])
                        table.insert(straightCards, cards[i+3])
                        table.insert(straightCards, cards[i+4])
                        result.isStraight = true
                        result.combine = straightCards
                        break --有結果就打破迴圈
                    end
                end
            end
        end
    end

    return result
end
--測三條
function CardManager.IsHaveThreeOfKind(sevenCards, result)
    --先複製一份副本
    local copyCards = {}
    for i, card in ipairs(sevenCards) do
        table.insert(copyCards, sevenCards[i])
    end
    
    local threeOfKindCards = {}
    --要找到第五張, 找到後取剩下的前面兩張
    for i = 1, 5, 1 do
        if (sevenCards[i].num == sevenCards[i+1].num) and
            (sevenCards[i+1].num == sevenCards[i+2].num) then
            table.insert(threeOfKindCards, sevenCards[i])
            table.insert(threeOfKindCards, sevenCards[i+1])
            table.insert(threeOfKindCards, sevenCards[i+2])
            --從副本移除(要從後面刪除)
            table.remove(copyCards, i+2)
            table.remove(copyCards, i+1)
            table.remove(copyCards, i)
            --把剩下的前面兩張放進去
            table.insert(threeOfKindCards, copyCards[1]) --一定是剩下的牌中最大的
            table.insert(threeOfKindCards, copyCards[2]) --一定是剩下的牌中第二大的
            result.isThreeOfKind = true
            result.combine = threeOfKindCards
            break --有結果打破迴圈, 組合可能是[7,7,7,6,5] or [2,2,2,A,Q], 到時候判斷從第四張看
        end
    end
    
    return result
end
--測兩對
function CardManager.IsHaveTwoPair(sevenCards, result)
    --先複製一份副本
    local copyCards = {}
    for i, card in ipairs(sevenCards) do
        table.insert(copyCards, sevenCards[i])
    end

    local twoPairCards = {}
    --分兩次找, 先找出一對, 再找出剩下一對, 最後把副本第一張牌加入
    --找到第四張，還沒找到一對，不用找了
    for i = 1, 4, 1 do
        if sevenCards[i].num == sevenCards[i+1].num then
            --找到一對
            table.insert(twoPairCards, sevenCards[i])
            table.insert(twoPairCards, sevenCards[i+1])
            --從副本移除(要從後面刪除)
            table.remove(copyCards, i+1)
            table.remove(copyCards, i)
            --從剩下的找最後一對
            for j = 1, 4, 1 do
                if (copyCards[j].num == copyCards[j+1].num) then
                    table.insert(twoPairCards, copyCards[j])
                    table.insert(twoPairCards, copyCards[j+1])
                    --從副本移除(要從後面刪除)
                    table.remove(copyCards, j+1)
                    table.remove(copyCards, j)
                    --找到兩對了, 放最後一張
                    table.insert(twoPairCards, copyCards[1]) --一定是剩下的牌中最大的
                    result.isTwoPair = true
                    result.combine = twoPairCards
                    break --有結果就打破迴圈
                end
            end
            break --找到一對, 剩下的還沒找到也打破迴圈
        end
    end

    return result
end
--測一對
function CardManager.IsHaveOnePair(sevenCards, result)
    --先複製一份副本
    local copyCards = {}
    for i, card in ipairs(sevenCards) do
        table.insert(copyCards, sevenCards[i])
    end
    --要找到第六張
    local onePairCards = {}
    for i = 1, 6, 1 do
        if sevenCards[i].num == sevenCards[i+1].num then
            table.insert(onePairCards, sevenCards[i])
            table.insert(onePairCards, sevenCards[i+1])
            --從副本移除(要從後面刪除)
            table.remove(copyCards, i+1)
            table.remove(copyCards, i)
            --把剩下三張放進去
            table.insert(onePairCards, copyCards[1]) --一定是剩下的牌中最大的
            table.insert(onePairCards, copyCards[2]) --一定是剩下的牌中第二大的
            table.insert(onePairCards, copyCards[3]) --一定是剩下的牌中第三大的
            result.isOnePair = true
            result.combine = onePairCards
            break --有結果就打破迴圈
        end
    end
    
    return result
end
-- 回傳A是否大於B
function CardManager.IsCardABiggerThanCardB(cardA, cardB)
    if cardA.num ~= cardB.num then
        --有一張是Ace
        if cardA.num == 1 or cardB.num == 1 then
            if cardA.num < cardB.num then
                return true
            else
                return false
            end
        else
        --都不是Ace
            if cardA.num > cardB.num then
                return true
            else
                return false
            end
        end
    else
        --數字一樣
        local a_suit = CardManager.GetSuitNum(cardA.suit)
        local b_suit = CardManager.GetSuitNum(cardB.suit)
        if a_suit > b_suit then
            return true
        else
            return false
        end
    end
end

function CardManager.GetSuitNum(strSuit)
    if strSuit == "spade" then
        return 4
    end
    if strSuit == "heart" then
        return 3
    end
    if strSuit == "diamond" then
        return 2
    end
    if strSuit == "club" then
        return 1
    end
    return 0
end

return CardManager