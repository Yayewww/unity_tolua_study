local GameSetConfig = 
{
    --敵人總存活秒數為 (actionTime) + (i * 1.2) + (math.random(1.25, 1.75))
    --最小總秒數 = (actionTime) + 1.2 + 1.25 = 2.45 + actionTime
    --則action不應小於0.6
    {
        enemyNum = 4,
        blind = 50,
        actionTime = 1.8,
    },
    {
        enemyNum = 4,
        blind = 75,
        actionTime = 1.8,
    },
    {
        enemyNum = 4,
        blind = 100,
        actionTime = 1.8,
    },
    {
        enemyNum = 4,
        blind = 125,
        actionTime = 1.5,
    },
    {
        enemyNum = 4,
        blind = 150,
        actionTime = 1.5,
    },
    {
        enemyNum = 4,
        blind = 200,
        actionTime = 1.5,
    },
    {
        enemyNum = 4,
        blind = 250,
        actionTime = 1,
    },
    {
        enemyNum = 4,
        blind = 300,
        actionTime = 1,
    },
    {
        enemyNum = 4,
        blind = 400,
        actionTime = 1,
    },
    {
        enemyNum = 4,
        blind = 1000,
        actionTime = 0.6,
    },
}

return GameSetConfig