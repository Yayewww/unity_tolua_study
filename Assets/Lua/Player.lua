Player = {}
local this = Player
local player
local rigi


function this.Awake(obj)
    player = obj
    rigi = player:GetComponent('Rigidbody')
end

function this.StartJump(time)
    rigi:AddForce(Vector3(50, 50, 0) * time * 7, E_ForceMode.Impulse)
end

function this.Update()
    if Input.GetKeyDown(KeyCode.Space) then
        startTime = Time.time
    end
    if Input.GetKey(KeyCode.Space) then
        if player.transform.localScale.y > 0.11 then
            player.transform.localScale = player.transform.localScale + Vector3(1, -1, 1) * 0.5 * Time.deltaTime
        end
    end
    if Input.GetKeyUp(KeyCode.Space) then
        player.transform.localScale = Vector3(1, 1, 1)
        endTime = Time.time - startTime
        this.StartJump(endTime)
        G_UIEvent.TestLuaCall()
    end
end