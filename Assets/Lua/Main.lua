--[[ 
    語法簡易說明:
    "."號 :
        variable.property
        variable.staticfunc()
    
    ":"號 :
        variable:memberfunc()

    範例1 :
    local obj = GameObject.Find("obj_name")
    ↑ 用的是UnityEngine.GameObject的靜態方法來取得obj, 所以obj是GameObject類

    local obj_child = obj.transform:Find("child_name")
    ↑ 用的是obj的transform的成員方法來取得obj_child, 所以obj_child是Transform類

    local component_text = obj_child.gameObject:GetComponent('Text')
    ↑ 注意用的時候當前的變數是屬於哪個類別, Transform也可以GetComponent();

    component_text.text = "顯示在Text.text的string"
    ↑ 這樣設置是可以的.
    
    範例2 :
    local transform_obj = obj:GetComponent(type(UnityEngine.Transform))
    ↑ tolua要用泛型，就是把你要的類別弄成字串當參數丟進去, 這樣做來確保沒打錯字
    ↑ 注意類別要有綁定導出到lua才能用

    跟C#很像, 用的時候都要回頭看一下C#的代碼
    ------
    宣告區域變數時，個人建議都小寫用底線連接 :
        local 類別名_物件名 = ......

    全域的變數 :
        G_類別名_物件名 = ......

    有放到模塊的 :
        self.類別名_物件名 = .......

    預設是換場景才gc, 下方有個collectgarbage("collect"), 注意物件生命週期管理
    範例3 :
    type vs typeof

	GameObject = UnityEngine.GameObject
    type(GameObject) <- 拿到table, 也就是lua的數據類型
    typeof(GameObject) <- 拿到userdata, 也就是Unity的數據類型
]]
--主入口函数。从这里开始lua逻辑
function Main()
    --------Unity Class--------
	GameObject = UnityEngine.GameObject
    Transform = UnityEngine.Transform
    ParticleSystem = UnityEngine.ParticleSystem
    Color = UnityEngine.Color
    SceneManagement = UnityEngine.SceneManagement
    Input = UnityEngine.Input
    KeyCode = UnityEngine.KeyCode
    Time = UnityEngine.Time
    Camera = UnityEngine.Camera
    AudioSource = UnityEngine.AudioSource
    Resources = UnityEngine.Resources
    ForceMode = UnityEngine.ForceMode
    RectTransform = UnityEngine.RectTransform
    Text = UnityEngine.UI.Text
    Image = UnityEngine.UI.Image
    Ease = DG.Tweening.Ease
    www = UnityEngine.WWW
    -------------自定義Class-------------------
	G_Util = Util
	G_UIEvent = UIEvent
    G_FireBaseManager = FirebaseManager
    -------------自定義Lua模塊------------------
    G_CardManager = require("CardManager")
    ---------------------------------------
    print("Lua 邏輯開始")
end

function ReadyToEnterLobby()
    G_GameManager = GameObject.Find("GameManager")
    local canvas = G_GameManager.transform:GetComponentInChildren(typeof(UnityEngine.Canvas))
    local panel_finishload = canvas.transform:Find("Panel_FinishLoad")
    local obj_loadfinish = panel_finishload.transform:Find("Btn_LoadFinish").gameObject
    local function OnClick()
        print("點擊進入Lobby")
        obj_loadfinish.gameObject:SetActive(false)
        G_UIEvent.OnClickToLoadScene("Lobby")
    end
    G_UIEvent.AddButtonOnClick(obj_loadfinish, OnClick)
end

--场景切换通知
function OnLevelWasLoaded(level)
	collectgarbage("collect") --強制GC
	Time.timeSinceLevelLoad = 0
	print("OnLevelWasLoaded", level)
end

function OnApplicationQuit()

end