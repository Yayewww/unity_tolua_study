GameScene = {}
--用Scene來綁GameObject
function GameScene.Start(obj)
    print("=====GameScene.Start()=====")
    --[綁該場景的ObjectManager]
    local objectManager = obj:GetComponent(typeof(ObjectManager))
    --[綁52張牌ui]
    --NOTE : 下面取出來是個Transform array, array[i] 是可以用的, 但數據類型還是userdata, 可以用 :ToTable()這個方式轉成lua的table
    --NOTE : 如果沒轉成table的話, 序號一樣是從0開始, 反之轉成table後, 從1開始
    --NOTE : 這裡故意不轉成table是因為obj_cards[0]是canvas自己，所以剛好從1開始是卡片
    local Card_Canvas = GameObject.Find("Card_Canvas")
    local obj_cards = Card_Canvas:GetComponentsInChildren(typeof(UnityEngine.Transform))
    for i = 1, 52, 1 do
        obj_cards[i].gameObject:SetActive(false)
    end
    --NOTE : 把所有卡片對象放入table綁上key, key = obj.name
    local ui_cards_table = G_CardManager.InitUICardsTable(obj_cards)
    --[5張公共背牌]
    local BackCard_Canvas = GameObject.Find("BackCard_Canvas")
    local back_common_cards = BackCard_Canvas:GetComponentsInChildren(typeof(UnityEngine.Transform))
    --[GameView]
    local params = {}
    params.objectManager = objectManager
    params.Card_Canvas = Card_Canvas
    params.ui_cards = ui_cards_table
    params.back_common_cards = back_common_cards
    local GameView = require("GameView").new(params)
end

return GameScene