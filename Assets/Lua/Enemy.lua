local Enemy = class("Enemy")
local EaseType = require("EaseType")

function Enemy:ctor(params)
    self.rootObj = params.obj
    self.parent = params.parent
    self.Card_Canvas = params.Card_Canvas
    self.startPos = params.startPos
    self.blind = params.blind
    self.moveTime = params.moveTime
    self.OnEnemyGoal = params.OnEnemyGoal
    self.alive = true

    self.rootObj.gameObject:SetActive(true)
    self.rootObj.transform:SetParent(self.parent)
    --綁
    self.Img_Idle = self.rootObj.transform:Find("Img_Idle").gameObject
    self.Img_Win = self.rootObj.transform:Find("Img_Win").gameObject
    self.Img_Lose = self.rootObj.transform:Find("Img_Lose").gameObject
    self.Text_Blind = self.rootObj.transform:Find("Text_Blind").gameObject
    self.Back_Card_1 = self.rootObj.transform:Find("Back_Card_1").gameObject
    self.Back_Card_2 = self.rootObj.transform:Find("Back_Card_2").gameObject
    self.Back_Card_1:SetActive(true)
    self.Back_Card_2:SetActive(true)
    self.UI_Hand_Card_1 = {}
    self.UI_Hand_Card_2 = {}

    self:Init()
end

function Enemy:Init()
    self.rootObj:GetComponent(typeof(RectTransform)).anchoredPosition  = self.startPos
    self.Text_Blind:GetComponent(typeof(Text)).text = self.blind
    self:Go()
end

function Enemy:Go()
    self:Idle()
    local function OnGoal()
        self:KillSelf()
        local damage = self:GetBlind()
        self.OnEnemyGoal(damage)
    end
    local pos = Vector3(self.startPos.x, -300)
    G_Util.DoMoveAnchor(self.rootObj.gameObject, pos, self.moveTime, OnGoal, Ease.Linear)
    self.Text_Blind:GetComponent(typeof(Text)):DOCounter(self.blind, (self.blind * 3), self.moveTime)
end

function Enemy:SetUIHandCards(ui_card1, ui_card2)
    self.UI_Hand_Card_1 = ui_card1
    self.UI_Hand_Card_2 = ui_card2
    self.UI_Hand_Card_1:SetParent(self.rootObj.transform)
    self.UI_Hand_Card_2:SetParent(self.rootObj.transform)
    self.UI_Hand_Card_1:GetComponent(typeof(RectTransform)).anchoredPosition  = self.Back_Card_1:GetComponent(typeof(RectTransform)).anchoredPosition 
    self.UI_Hand_Card_2:GetComponent(typeof(RectTransform)).anchoredPosition  = self.Back_Card_2:GetComponent(typeof(RectTransform)).anchoredPosition 
end

function Enemy:Stop()
    self.rootObj:DOKill()
    self.Text_Blind:GetComponent(typeof(Text)):DOKill()
end

function Enemy:ShowHandCards()
    self.Back_Card_1:SetActive(false)
    self.Back_Card_2:SetActive(false)
    self.UI_Hand_Card_1.gameObject:SetActive(true)
    self.UI_Hand_Card_2.gameObject:SetActive(true)
end

function Enemy:GetBlind()
    self.blind = tonumber(self.Text_Blind:GetComponent(typeof(Text)).text)
    return self.blind
end

function Enemy:KillSelf()
    self:Stop()
    self.alive = false
    self.UI_Hand_Card_1:SetParent(self.Card_Canvas.transform)
    self.UI_Hand_Card_2:SetParent(self.Card_Canvas.transform)
    self.UI_Hand_Card_1.gameObject:SetActive(false)
    self.UI_Hand_Card_2.gameObject:SetActive(false)
    self.rootObj.gameObject:SetActive(false)
end

function Enemy:HideAllFace()
    self.Img_Idle:SetActive(false)
    self.Img_Win:SetActive(false)
    self.Img_Lose:SetActive(false)
end

function Enemy:Idle()
    self:HideAllFace()
    self.Img_Idle:SetActive(true)
end

function Enemy:Win()
    self:HideAllFace()
    self.Img_Win:SetActive(true)
end

function Enemy:Lose()
    self:HideAllFace()
    self.Img_Lose:SetActive(true)
end

return Enemy