local GameView = class("GameView")
local Dealer = require("Dealer")
local GameSetConfig = require("GameSetConfig")

function GameView:ctor(params)
    self.objectManager = params.objectManager
    self.Card_Canvas = params.Card_Canvas
    self.ui_cards = params.ui_cards --是Transform[]
    self.back_common_cards = params.back_common_cards --是Transform[]

    self.Player_Canvas = self.objectManager:GetObjectFromMap("Player_Canvas")
    self.Text_CombineName = self.objectManager:GetObjectFromMap("Text_CombineName")
    self.Btn_All_In = self.objectManager:GetObjectFromMap("Btn_All_In")
    self.Btn_Ready = self.objectManager:GetObjectFromMap("Btn_Ready")
    self.Btn_Fold = self.objectManager:GetObjectFromMap("Btn_Fold")
    self.ui_chips = self.objectManager:GetObjectFromMap("ui_chips")
    self.Btn_Pause = self.objectManager:GetObjectFromMap("Btn_Pause")
    self.PauseMenu = self.objectManager:GetObjectFromMap("Panel_PauseMenu")
    self.Btn_BackLobby = self.objectManager:GetObjectFromMap("Btn_BackLobby")
    self.Btn_Restart = self.objectManager:GetObjectFromMap("Btn_Restart")
    self.ui_addChips = self.objectManager:GetObjectFromMap("ui_addChips")
    self.ui_minusChips = self.objectManager:GetObjectFromMap("ui_minusChips")
    self.Enemy_Canvas = self.objectManager:GetObjectFromMap("Enemy_Canvas")
    self.Text_Wave = self.objectManager:GetObjectFromMap("Text_Wave")
    self.Text_WinLose = self.objectManager:GetObjectFromMap("Text_WinLose")
    self.Text_StartBlind = self.objectManager:GetObjectFromMap("Text_StartBlind")
    self.Text_HighestScore = self.objectManager:GetObjectFromMap("Text_HighestScore")
    self.Text_GameOver = self.objectManager:GetObjectFromMap("Text_GameOver")

    self.ui_enemys = {}
    table.insert(self.ui_enemys, self.Enemy_Canvas.transform:Find("Enemy_1"))
    table.insert(self.ui_enemys, self.Enemy_Canvas.transform:Find("Enemy_2"))
    table.insert(self.ui_enemys, self.Enemy_Canvas.transform:Find("Enemy_3"))
    table.insert(self.ui_enemys, self.Enemy_Canvas.transform:Find("Enemy_4"))

    self:Init()
end

function GameView:Init()
    self.addChips = 0
    self.minusChips = 0
    self.myChips = 5000
    self.highestScore = 0
    self.openIndex = 1
    self.myPlayer = {}
    self.Enemys = {}
    self.IsPause = false
    self.handCardPos_1 = Vector3(-60, -800, 0)
    self.handCardPos_2 = Vector3(60, -800, 0)
    self.commonCardsPos = 
    {
        Vector3(-200, 840, 0),
        Vector3(-100, 840, 0),
        Vector3(0, 840, 0),
        Vector3(100, 840, 0),
        Vector3(200, 840, 0),
    }
    self.enemysPos = 
    {
        Vector3(130, 500),
        Vector3(-130, 500),
        Vector3(400, 500),
        Vector3(-400, 500),
    }
    --讀取本地存檔
    local saveDataStr = G_Util.ReadScore()
    if saveDataStr ~= "none" then
        self.highestScore = tonumber(saveDataStr)
    end

    self.currentWave = 1
    self:RefreshChips()
    --掛按鈕事件
    --Ready
    G_UIEvent.AddButtonOnClick(self.Btn_Ready, function ()
        self:OnReadyClick()
    end)
    --All-In
    G_UIEvent.AddButtonOnClick(self.Btn_All_In, function ()
        self:OnAllInClick() 
    end)
    --Fold
    G_UIEvent.AddButtonOnClick(self.Btn_Fold, function ()
        self:OnFoldClick()
    end)
    --Pause
    G_UIEvent.AddButtonOnClick(self.Btn_Pause, function ()
        self:OnPauseClick()
    end)
    --BackLobby
    G_UIEvent.AddButtonOnClick(self.Btn_BackLobby, function ()
        G_UIEvent.OnClickToLoadScene("Lobby")
    end)
    --Restart
    G_UIEvent.AddButtonOnClick(self.Btn_Restart, function ()
        G_UIEvent.OnClickToLoadScene("Game")
    end)
end

----------------[按鈕事件實現]---------------------
function GameView:OnReadyClick()
    --初始化設置
    self:Reset()
    if self.currentWave > #GameSetConfig then
        self.currentWave = #GameSetConfig
    end
    --載入關卡配置
    local config = GameSetConfig[self.currentWave]

    --Wave & Blind UI刷新
    self.Text_Wave:GetComponent(typeof(Text)).text = "Wave : "..self.currentWave
    self.Text_StartBlind:GetComponent(typeof(Text)).text = "Blind : "..config.blind
    print("Wave: ", self.currentWave)

    --設置玩家&敵人
    self.totalPlayers = {}
    table.insert(self.totalPlayers, self.myPlayer)
    self.Enemys = self:CreateEnemys(config)
    for i, enemy in ipairs(self.Enemys) do
        table.insert(self.totalPlayers, enemy)
    end

    --設置發牌Dealer
    local params = {}
    params.players = self.totalPlayers
    self.currentDealer = nil
    self.currentDealer = Dealer.new(params)

    --發牌
    self.currentDealer:GiveCommonCards()
    self.currentDealer:GiveCards()

    --我的手牌UI
    local ui_card1 = self:GetUICard(self.myPlayer.handCard[1])
    local ui_card2 = self:GetUICard(self.myPlayer.handCard[2])
    ui_card1:GetComponent(typeof(RectTransform)).anchoredPosition  = self.handCardPos_1
    ui_card2:GetComponent(typeof(RectTransform)).anchoredPosition  = self.handCardPos_2
    ui_card1.gameObject:SetActive(true)
    ui_card2.gameObject:SetActive(true)

    --敵人手牌UI
    for i, enemy in ipairs(self.Enemys) do
        local card_1 = self:GetUICard(enemy.handCard[1])
        local card_2 = self:GetUICard(enemy.handCard[2])
        enemy:SetUIHandCards(card_1, card_2)
    end

    --顯示公共牌背牌
    self:ShowCommonCardsBack(true)

    --按鈕切換
    self.Btn_Ready:SetActive(false)
    self.Btn_All_In:SetActive(true)
    self.Btn_Fold:SetActive(true)  

    --公共牌開牌倒數 (1秒開一張)
    self:ShowCommonCardsVisible(3)
end

function GameView:OnAllInClick()
    --以當前存活玩家計算
    local aliveEnemys = {}
    for i, enemy in ipairs(self.Enemys) do
        if enemy.alive then
            table.insert(aliveEnemys, enemy)
        end
    end
    self.currentDealer:AdjustWinner(self.myPlayer, aliveEnemys)
    --關閉公共牌背牌
    self:ShowCommonCardsBack(false)
    --公共牌開牌
    local commonCards = self.currentDealer.commonCards
    for i = 1, 5, 1 do
        local card = self:GetUICard(commonCards[i])
        card:GetComponent(typeof(RectTransform)).anchoredPosition  = self.commonCardsPos[i]
        card.gameObject:SetActive(true)
    end
    --敵人開牌
    for i, enemy in ipairs(self.Enemys) do
        if enemy.alive then
            enemy:ShowHandCards()
        end
    end
    --顯示自己的最大牌型
    local combineName = self.currentDealer:GetMyPlayerCombineName()
    self.Text_CombineName:GetComponent(typeof(Text)).text = combineName    
    --勝負分曉
    local winner = self.currentDealer.winner
    local combine = winner.combine
    local pot = 0 --底池
    for i, enemy in ipairs(self.Enemys) do
        if enemy.alive then
            pot = pot + enemy:GetBlind()
        end
    end
    self.Text_WinLose:SetActive(true)
    --平手
    if winner.isChop == true then
        print("平手")
        self.Text_WinLose:GetComponent(typeof(Text)).text = "Draw"
        self:DoChop()
    --玩家贏了    
    elseif winner.player_index == 1 then
        print("贏了")
        self.Text_WinLose:GetComponent(typeof(Text)).text = "You Win"
        self:DoWin(pot)
    --輸了
    else
        print("輸了")
        self.Text_WinLose:GetComponent(typeof(Text)).text = "You Lose"
        self:DoLose(pot)
    end
    self:HighlightWinnerCards(combine)
    self:DoNextHand()
end

function GameView:OnFoldClick()
    local blind = GameSetConfig[self.currentWave].blind
    local playersNum = GameSetConfig[self.currentWave].enemyNum + 1
    self:DoDamage(blind)
    self:DoNextHand()
end

function GameView:OnPauseClick()
    if self.IsPause == false then
        self.PauseMenu:SetActive(true)
        self.IsPause = true
    else
        self.PauseMenu:SetActive(false)
        self.IsPause = false
    end
end

function GameView:OnEnemyGoal(damage)
    local num = GameSetConfig[self.currentWave].enemyNum
    self:DoDamage(damage)
    --沒敵人時換回READY
    local aliveEnemys = 0
    for i, enemy in ipairs(self.Enemys) do
        if enemy.alive then
            aliveEnemys = aliveEnemys + 1
        end
    end
    if aliveEnemys == 0 then
        self:DoNextHand()
    end
    --一條命開一張牌
    -- local index = self.openIndex
    -- local commonCards = self.currentDealer.commonCards
    -- local ui_card = self:GetUICard(commonCards[index])
    -- ui_card:GetComponent(typeof(RectTransform)).anchoredPosition  = self.commonCardsPos[index]
    -- ui_card.gameObject:SetActive(true)
    -- self.openIndex = self.openIndex + 1
end

----------------[玩家行動後事件]---------------------
function GameView:DoChop()
    --某個特效
end

function GameView:DoWin(pot)
    --要贏才可以進下個wave
    self.currentWave = self.currentWave + 1
    self:AddChips(pot)
    for i, enemy in ipairs(self.Enemys) do
        if enemy.alive then
            enemy:Lose()
        end
    end
end

function GameView:DoLose(pot)
    self:DoDamage(pot)
    for i, enemy in ipairs(self.Enemys) do
        if enemy.alive then
            enemy:Win()
        end
    end
end

function GameView:DoDamage(damage)
    self:MinusChips(damage)
end

function GameView:DoNextHand()
    self:StopCommonCardsDOTween()
    self:StopEnemyDOTween()
    self.Btn_All_In:SetActive(false)
    self.Btn_Fold:SetActive(false)
    if self.myChips <= 0 then
        self:DoGameOver()
    else
        G_Util.DoFunctionDelay(self.Player_Canvas, 1, function ()
            self.Btn_Ready:SetActive(true)
        end)
    end
end

function GameView:DoGameOver()
    self:StopCommonCardsDOTween()
    self:StopEnemyDOTween()
    self.Btn_All_In:SetActive(false)
    self.Btn_Fold:SetActive(false)
    self.Btn_Ready:SetActive(false)
    self.Text_GameOver:SetActive(true)
    self:OnPauseClick()
end

----------------[UI處理]---------------------
function GameView:AddChips(chips)
    self.addChips = self.addChips + chips
    self.myChips = self.myChips + chips
    self.ui_addChips:SetActive(true)
    self.ui_addChips:GetComponent(typeof(Text)).text = "+"..tostring(self.addChips)
    self:RefreshChips()
end

function GameView:MinusChips(chips)
    self.minusChips = self.minusChips + chips
    self.myChips = self.myChips - chips
    self.ui_minusChips:SetActive(true)
    self.ui_minusChips:GetComponent(typeof(Text)).text = "-"..tostring(self.minusChips)
    self:RefreshChips()
end

function GameView:RefreshChips()
    self.ui_chips:GetComponent(typeof(Text)).text = tostring(self.myChips)
    if self.myChips > self.highestScore then
        self.highestScore = self.myChips
        local score = tostring(self.highestScore)
        G_Util.SaveScore(score)
    end
    if self.myChips <= 0 then
        self:DoGameOver()
    end
    self.Text_HighestScore:GetComponent(typeof(Text)).text = "最高紀錄 : "..self.highestScore
end

function GameView:CreateEnemys(config)
    local Enemys = {}
    math.randomseed(os.time())
    local startPosIndex = math.random(1,4)
    for i = 1, config.enemyNum, 1 do
        local posIndex = (startPosIndex + i) % config.enemyNum + 1
        local params = {}
        params.obj = self.ui_enemys[i] --ui物件
        params.parent = self.Enemy_Canvas.transform
        params.Card_Canvas = self.Card_Canvas
        params.startPos = self.enemysPos[posIndex]
        params.blind = config.blind
        params.moveTime = config.actionTime + (i * 1.2) + (math.random(1.25, 1.75))
        params.OnEnemyGoal = function (damage)
            self:OnEnemyGoal(damage)
        end
        local Enemy = require("Enemy").new(params)
        table.insert(Enemys, Enemy)
    end
    return Enemys
end

function GameView:StopEnemyDOTween()
    for i, enemy in ipairs(self.Enemys) do
        enemy:Stop()
    end
end

function GameView:ShowCommonCardsVisible(showNumbers) -- 要顯示到第幾張
    -- 可見的公共牌
    local commonCards = self.currentDealer.commonCards
    local ui_common_cards = {}
    self.TweeningCommonCards = {}
    for i = 1, showNumbers, 1 do
        local card = commonCards[i]
        local ui_card = self:GetUICard(card)
        ui_card:GetComponent(typeof(RectTransform)):DOAnchorPos(self.commonCardsPos[i], i):OnComplete(function ()
            ui_card.gameObject:SetActive(true)
        end)
        table.insert(self.TweeningCommonCards, ui_card)
    end
end

function GameView:StopCommonCardsDOTween()
    for i, ui_card in ipairs(self.TweeningCommonCards) do
        ui_card:GetComponent(typeof(RectTransform)):DOKill()
    end
end

function GameView:ShowCommonCardsBack(bShow)
    --公共牌蓋牌
    for i = 1, 5, 1 do
        self.back_common_cards[i].gameObject:SetActive(bShow)
        self.back_common_cards[i].anchoredPosition  = self.commonCardsPos[i]
    end
end

function GameView:Reset()
    self.addChips = 0
    self.minusChips = 0
    self.openIndex = 1

    self.ui_addChips:SetActive(false)
    self.ui_minusChips:SetActive(false)
    self.Text_WinLose:SetActive(false)
    --重置UI卡牌
    for key, card in pairs(self.ui_cards) do
        card:GetComponent(typeof(Image)).color = Color(1,1,1,1)
        card.anchoredPosition  = Vector3(-100, 0, 0)
        card.gameObject:SetActive(false)
    end
    --重置牌型Text
    self.Text_CombineName:GetComponent(typeof(Text)).text = "----"
    --重置上組敵人
    for i, enemy in ipairs(self.Enemys) do
        enemy:KillSelf()
    end
end

function GameView:HighlightWinnerCards(combine)
    --高亮贏家牌型
    for i, card in ipairs(combine) do
        local ui_card = self:GetUICard(card)
        ui_card:GetComponent(typeof(Image)).color = Color(1, 0.9, 0.7, 1)
    end
end

----------------[Getter]---------------------
function GameView:GetUICard(card)
    local key = "c_"..card.suit.."_"..card.num
    return self.ui_cards[key]
end

return GameView