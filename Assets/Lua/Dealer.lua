local Dealer = class("Dealer")

function Dealer:ctor(params)
    self.cards = G_CardManager.NewCards()
    self.myChips = 0
    self.players = params.players -- 1是玩家自己
    self.commonCards = {}
    self.myPlayerCombineName = ""
end

function Dealer:GiveCommonCards()
    -- print("----公共牌----")
    for i = 1, 5, 1 do
        table.insert(self.commonCards, self.cards[1])
        table.remove(self.cards, 1)
        print(self.commonCards[i].suit, self.commonCards[i].num)
    end
    return self.commonCards
end

function Dealer:GiveCards()
    for i, player in ipairs(self.players) do
        player.handCard = {}
        table.insert(player.handCard, self.cards[1])
        table.insert(player.handCard, self.cards[2])
        --從後面刪除
        table.remove(self.cards, 2)
        table.remove(self.cards, 1)
        -- if i == 1 then
        --     print("---我的牌---")
        -- else
        --     print("--敵人 "..(i - 1).." 的牌--")
        -- end
        -- print(player.handCard[1].suit, player.handCard[1].num)
        -- print(player.handCard[2].suit, player.handCard[2].num)
    end
end

function Dealer:AdjustWinner(myPlayer, enemys)
    local winner = 
    {
        isChop = false,
        player_index = 0,
        hand_ranking = 0,
        hand_name = "",
        combine = {},
    }
    --發生平手時, 紀錄該平手牌型
    local chop_combine = {}
    --當前存活玩家
    local totalPlayers = {} -- 1是玩家自己
    table.insert(totalPlayers, myPlayer)
    for i, enemy in ipairs(enemys) do
        table.insert(totalPlayers, enemy)
    end

    --先幫每個玩家得出最佳牌型
    for i, player in ipairs(totalPlayers) do
        local sevenCards = {}
        --兩張私牌 + 五張公牌
        table.insert(sevenCards, player.handCard[1])
        table.insert(sevenCards, player.handCard[2])
        for i = 1, 5, 1 do
            table.insert(sevenCards, self.commonCards[i])
        end

        player.result = G_CardManager.GetBiggestHandRanking(sevenCards)
        if i == 1 then
            self:SetMyPlayerCombineName(player.result.hand_name)
        end
        local combine = player.result.combine
        local str = "[combine] "..i.." :"
        for j, card in ipairs(combine) do
            str = str.."\n"..card.suit.."|"..card.num
        end
        print(str)
    end

    --比較每個牌型誰最佳
    for index, player in ipairs(totalPlayers) do
        local result = player.result

        if result.hand_ranking > winner.hand_ranking then --比較者牌型較大，取代贏家
            winner.player_index = index
            winner.hand_ranking = result.hand_ranking
            winner.hand_name = result.hand_name
            winner.combine = result.combine
        elseif result.hand_ranking == winner.hand_ranking then --牌型一樣，比五張
            for j = 1, 5, 1 do
                local playerCard = result.combine[j]
                local winnerCard = winner.combine[j]
                --兩張牌大小不一樣，就出結果了
                if playerCard.num ~= winnerCard.num then 
                    if playerCard.num == 1 or winnerCard.num == 1 then --做有Ace的判斷
                        --比較者的Ace比較大, 直接換掉當前贏家，反之贏家不變
                        if playerCard.num < winnerCard.num then
                            winner.player_index = index
                            winner.hand_ranking = result.hand_ranking
                            winner.hand_name = result.hand_name
                            winner.combine = result.combine
                        end
                    elseif playerCard.num > winnerCard.num then --非Ace的情況
                        winner.player_index = index
                        winner.hand_ranking = result.hand_ranking
                        winner.hand_name = result.hand_name
                        winner.combine = result.combine
                    end

                    break --跳出比對迴圈
                else
                    --兩張一樣，且已經第五張了
                    if j == 5 then
                        print("紀錄平手牌型")
                        chop_combine = result.combine
                        for i, card in ipairs(chop_combine) do
                            print(card.suit.."|"..card.num)
                        end
                    end
                end
            end
        end
    end


    --平手牌型是否跟贏家牌型一樣
    if #chop_combine > 0 then
        local isSame = true
        for i = 1, 5, 1 do
            if chop_combine[i].num ~= winner.combine[i].num then
                isSame = false
                break
            end
        end
        --確定發生平手, 判斷當前贏家是不是玩家自己, 是的話就當平手處理，但如果是CPU vs CPU 平手, 一樣當玩家輸
        if isSame and winner.player_index == 1 then
            winner.isChop = true
            print("玩家vsCPU 平手")
        end
        --沒平手代表有贏家
    end
    print("贏家:", winner.player_index, winner.hand_name)
    self.winner = winner
end

function Dealer:SetMyPlayerCombineName(name)
    self.myPlayerCombineName = name
end

function Dealer:GetMyPlayerCombineName()
    return self.myPlayerCombineName
end


return Dealer